import tkinter as tk

win = tk.Tk()


canvas = tk.Canvas(win, bg="white", width=500, height=500)


for i in range(0, 251, 10):
    canvas.create_line(0, i, 250, 250 - i, fill="red")


for h in range(
    0,
    250,
    10,
):
    canvas.create_line(250, h, 500 - h, 250, fill="blue")
for j in range(0, 251, 10):
    canvas.create_line(500, j, 250 + j, 250, fill="blue")
for k in range(0, 250, 10):
    canvas.create_rectangle(k, 250 + k, k + 10, 260 + k, fill="green")
for l in range(0, 125, 15):
    canvas.create_oval(
        250 + l, 250 + l, 500 - l, 500 - l, fill="yellow", outline="black"
    )

canvas.pack()
win.mainloop()
