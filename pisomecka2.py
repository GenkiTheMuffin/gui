import tkinter as tk

count = 10
size = 20
win = tk.Tk()
canvas = tk.Canvas(win, width=205, height=405, bg="white")
canvas.pack()
first_click_item = None


def draw_grid():
    global size, count
    for y in range(4, size * count, size):
        for x in range(4, size * count, size):
            canvas.create_rectangle(
                x, y, x + size, y + size, outline="black", tags="square", fill="white"
            )


original_color = None


def action(event):
    global first_click_item
    global original_color

    items = canvas.find_overlapping(event.x, event.y, event.x + 1, event.y + 1)

    if first_click_item is None:
        first_click_item = items[0]
        original_color = canvas.itemcget(first_click_item, "fill")
        if original_color != "blue":
            canvas.itemconfig(first_click_item, fill="blue")
        return

    x1, y1, _, _ = canvas.coords(first_click_item)
    x2, y2, _, _ = canvas.coords(items[0])
    min_x, max_x = min(x1, x2), max(x1, x2)
    min_y, max_y = min(y1, y2), max(y1, y2)

    if min_x != max_x and min_y != max_y:
        min_x, min_y = min(min_x, x1), min(min_y, y1)
        max_x, max_y = max(min_x, x1), max(min_y, y1)

        canvas.itemconfig(first_click_item, fill=original_color)
        first_click_item = None
        original_color = None
        return

    for item in canvas.find_withtag("square"):
        x1, y1, _, _ = canvas.coords(item)
        if min_x <= x1 <= max_x and min_y <= y1 <= max_y:
            canvas.itemconfig(item, fill="blue")

    first_click_item = None


canvas.tag_bind("square", "<Button-1>", action)
draw_grid()
win.mainloop()
