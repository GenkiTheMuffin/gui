import tkinter as tk

count = 10
size = 20
win = tk.Tk()
canvas = tk.Canvas(win, width=205, height=405, bg="white")
canvas.pack()
entry = tk.Entry(win, bg="white")
entry.insert(0, "#FF0000")
entry.pack()


def draw_grid():
    global size, count
    for y in range(4, size * count, size):
        for x in range(4, size * count, size):
            canvas.create_rectangle(
                x, y, x + size, y + size, outline="black", tags="square", fill="white"
            )


def action(event):
    items = canvas.find_overlapping(event.x, event.y, event.x + 1, event.y + 1)
    canvas.itemconfig(items[0], fill=entry.get())


def save():
    output_file = open("level.txt", "w")
    for item in canvas.find_withtag("square"):
        x1, y1, x2, y2 = canvas.coords(item)
        color = canvas.itemcget(item, "fill")
        output_file.write(f"{x1},{y1},{x2},{y2},{color}\n")
    output_file.close()


canvas.tag_bind("square", "<Button-1>", action)
button = tk.Button(win, text="Save", bg="white", command=save)
button.pack()
draw_grid()
win.mainloop()
