import tkinter as tk

x, y = 250, 250
offset_x = 0
offset_y = -1


def keypress(event):
    global offset_y, offset_x
    if event.keysym == "h":
        offset_x, offset_y = -1, 0
    if event.keysym == "j":
        offset_x, offset_y = 0, 1
    if event.keysym == "k":
        offset_x, offset_y = 0, -1
    if event.keysym == "l":
        offset_x, offset_y = 1, 0


def move():
    global offset_x, offset_y, point
    global line
    canvas.move(point, offset_x, offset_y)
    canvas.create_oval(canvas.coords(point), fill="hotpink", outline="hotpink")

    canvas.after(100, move)


def check_collision(item1, item2):
    item1_bbox = canvas.bbox(item1)
    overlapping_items = canvas.find_overlapping(
        item1_bbox[0], item1_bbox[1], item1_bbox[2], item1_bbox[3]
    )

    if item2 in overlapping_items:
        print("Collision detected! Stopping program.")
        win.destroy()


win = tk.Tk()
canvas = tk.Canvas(win, width=500, height=500, bg="white")
point = canvas.create_oval(x, y, x + 1, y + 1, fill="hotpink", outline="hotpink")


line = canvas.create_line(x, y - offset_y, x, y, fill="hotpink")


move()

canvas.bind_all("<Key>", keypress)
canvas.focus_set()
canvas.pack()
win.mainloop()
