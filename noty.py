import tkinter as tk

noty = "cdefgahcdahdecdefgahhagfdec"
vyska_noty = 10
sirka_noty = 20
offset_y = 0
sirka_platna = 300
# c = 5
# d = 4
# e = 3.5
# f = 3
# g = 2.5
# a = 2
# h = 1.5
noty_kriznica = {"c": 6, "d": 5, "e": 4.5, "f": 4, "g": 3.5, "a": 3, "h": 2.5}
offset_x = 0


def kresli_osnovu(offset_y):
    for i in range(6):
        canvas.create_line(
            0,
            offset_y + vyska_noty * i,
            sirka_platna,
            offset_y + vyska_noty * i,
            fill="black",
        )


def kresli_noty(offset_x, offset_y):
    for nota in noty:

        canvas.create_oval(
            offset_x,
            offset_y + noty_kriznica[nota] * vyska_noty,
            offset_x + sirka_noty,
            vyska_noty * noty_kriznica[nota] + offset_y + vyska_noty,
            outline="black",
        )
        offset_x += 2 * sirka_noty
        if offset_x >= sirka_platna:
            offset_x = 0
            offset_y += 10 * vyska_noty
            kresli_osnovu(offset_y)


win = tk.Tk()
canvas = tk.Canvas(win, height=500, width=sirka_platna, bg="white")
canvas.pack()
kresli_osnovu(offset_y)
kresli_noty(offset_y, offset_x)
win.mainloop()
