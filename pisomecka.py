import tkinter as tk

win = tk.Tk()


canvas = tk.Canvas(win, bg="white", width=300, height=300)


for i in range(0, 151, 10):
    canvas.create_line(0 + i, 150 - i, 150, 150, fill="black")

for h in range(0, 151, 20):
    canvas.create_rectangle(215, 0 + h, 225, 10 + h, fill="blue", outline="black")
for j in range(0, 151, 20):
    canvas.create_rectangle(225, 10 + j, 235, 20 + j, fill="green", outline="black")
for k in range(0, 151, 10):
    canvas.create_line(0, 150 + k, 150, 225, fill="blue")
for l in range(0, 151, 10):
    canvas.create_line(150, 150 + l, 0, 225, fill="red")

for a in range(0, 151, 10):
    canvas.create_oval(
        150 + a, 150 + a, 160 + a, 160 + a, fill="yellow", outline="black"
    )
for b in range(0, 150, 10):
    canvas.create_oval(
        150 + b, 300 - b, 160 + b, 290 - b, fill="hotpink", outline="black"
    )

canvas.pack()
win.mainloop()
