# Maturujeme v pythone zadanie č43
import tkinter as tk
import random

colours = ["red", "blue", "hotpink", "grey", "orange"]

win = tk.Tk()

win.title("Pyrotechnik")

canvas = tk.Canvas(win, bg="white", width=500, height=500)

canvas.create_text(245, 10, text="Pyrotechnik", fill="blue")

colour = colours[random.randint(0, len(colours) - 1)]

kabel0 = canvas.create_rectangle(10, 220, 210, 230, fill=colour, outline="black")
colours.remove(colour)
colour = colours[random.randint(0, len(colours) - 1)]

kabel1 = canvas.create_rectangle(10, 230, 210, 240, fill=colour, outline="black")
colours.remove(colour)
colour = colours[random.randint(0, len(colours) - 1)]

kabel2 = canvas.create_rectangle(10, 240, 210, 250, fill=colour, outline="black")
colours.remove(colour)
colour = colours[random.randint(0, len(colours) - 1)]

kabel3 = canvas.create_rectangle(10, 250, 210, 260, fill=colour, outline="black")
colours.remove(colour)
colour = colours[random.randint(0, len(colours) - 1)]

kabel4 = canvas.create_rectangle(10, 260, 210, 270, fill=colour, outline="black")

tag_list = [kabel0, kabel1, kabel2, kabel3, kabel4]


def winner(tag_list):
    tag_id = tag_list[random.randint(0, len(tag_list) - 1)]
    return tag_id


winning_tag = winner(tag_list)


def winner_winner_chicken_dinner(event):
    text = canvas.create_text(
        100,
        450,
        text="Winner Winner Chicken Dinner!",
        fill="black",
    )


canvas.tag_bind(winning_tag, "<Button-1>", winner_winner_chicken_dinner)

canvas.pack()
win.mainloop()
