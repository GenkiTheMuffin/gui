import tkinter as tk

win = tk.Tk()
canvas = tk.Canvas(win, width=650, height=200, bg="white")
canvas.pack()
count = 15
player = 1


def zapalka(x, y):
    canvas.create_line(x, y, x, y + 100, width=5, fill="yellow", tags="zapalka")
    canvas.create_oval(
        x - 5, y - 5, x + 5, y + 8, fill="brown", outline="brown", tags="zapalka"
    )


def message(player, count):
    canvas.delete("message")
    canvas.create_text(
        325,
        170,
        text=f"taha hrac: {player}",
        anchor="center",
        font="Arial 25",
        tags="message",
        fill="black",
    )
    canvas.create_text(
        325,
        150,
        text=f"pocet zapaliek {count}",
        anchor="center",
        fill="black",
        font="Arial 25",
        tags="message",
    )


def draw(count):
    canvas.delete("zapalka")
    x = 25
    for i in range(count):
        zapalka(x * i + 10, 10)


def action(event):
    global count, player
    if count <= 1:
        canvas.delete("message")
        if player == 1:
            player = 2
        else:
            player = 2
        canvas.create_text(
            325,
            150,
            fill="hotpink",
            anchor="center",
            font="Arial 25",
            text=f"player {player} wins",
        )
    else:
        if event.char == "1":
            count -= 1
            draw(count)
        if event.char == "2":
            count -= 2
            draw(count)
        if event.char == "3":
            count -= 3
            draw(count)
        if int(event.char) > 3 or int(event.char) < 1:
            pass
        else:
            if player == 1:
                player = 2
                message(player, count)
            else:
                player = 1
                message(player, count)


draw(count)
message(player, count)
canvas.bind_all("<Key>", action)
win.mainloop()
