import tkinter as tk

win = tk.Tk()

canvas = tk.Canvas(win, width=500, height=500, bg="white")

stvorec = canvas.create_rectangle(200, 100, 400, 300, fill="red")
stvorec2 = canvas.create_rectangle(300, 200, 400, 300, fill="blue")


def action(event):
    print("Stalo sa to")
    print(event.x, event.y)
    objekty = canvas.find_overlapping(event.x, event.y, event.x + 1, event.y + 1)
    print(objekty)


# canvas.bind("<Button-1>", action)
canvas.tag_bind(stvorec, "<Button-1>", action)

canvas.pack()
win.mainloop()
