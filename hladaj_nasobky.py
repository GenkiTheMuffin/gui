import tkinter as tk
from random import randrange

M = randrange(3, 6)
N = randrange(3, 11)
squares = []
helper = {}
lfmulti = randrange(2, 10)


def create_scene():
    global squares, helper
    offset = 100
    canvas.create_text(
        10, 50, text="Hladaj nasobky cisla", font="Arial 15", anchor="nw", fill="black"
    )
    canvas.create_rectangle(200, 30, 250, 80, fill="limegreen")
    canvas.create_text(
        225,
        55,
        text=lfmulti,
        font="Arial 25",
        anchor="center",
        fill="black",
    )
    for i in range(M):
        for j in range(N):
            temp = canvas.create_rectangle(
                j * 55,
                i * 55 + offset,
                (j + 1) * 55 - 5,
                (i + 1) * 55 + offset - 5,
                fill="limegreen",
                tags="rectangle",
            )
            squares.append(temp)
            tempnum = randrange(1, 100)
            helper[temp] = tempnum
            canvas.create_text(
                j * 55 + 25,
                i * 55 + offset + 25,
                text=tempnum,
                anchor="center",
                font="Arial 25",
                fill="black",
                tags="number",
            )


def swap(items):
    canvas.itemconfig(items[0], fill="limegreen")


def action(event):
    items = canvas.find_overlapping(event.x, event.y, event.x + 1, event.y + 1)
    if items[0] in squares:
        number = helper[items[0]]
    if not number % lfmulti == 0:
        canvas.itemconfig(items[0], fill="tomato")
        canvas.after(100, swap, items)
    else:
        canvas.itemconfig(items[0], fill="peru")


win = tk.Tk()
canvas = tk.Canvas(width=600, height=500, bg="white")
create_scene()
canvas.tag_bind("rectangle", "<Button-1>", action)
canvas.tag_bind("number", "<Button-1>", action)
canvas.pack()
win.mainloop()
