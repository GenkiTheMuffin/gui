import tkinter as tk

win = tk.Tk()
input_file = open("zastavba_na_ulici.txt", "r")
builds = []


def get_delta_y():
    global delta_y
    delta_y = int(entry.get())
    offset = 0
    for i in range(len(builds) - 1):
        if abs(builds[i][1] - builds[i + 1][1]) >= delta_y:
            canvas.create_line(
                offset + builds[i][0],
                min(300 - builds[i][1], 300 - builds[i + 1][1]),
                offset + builds[i][0],
                max(300 - builds[i][1], 300 - builds[i + 1][1]),
                fill="red",
            )
        offset += builds[i][0]


def draw():
    offset = 0
    global dimmensions_num
    global builds
    for item in input_file:
        dimmensions = item.strip().split(" ")
        dimmensions_num = [int(i) for i in dimmensions]
        builds.append(dimmensions_num)
        if dimmensions_num[1] == 0:
            col = "lime"
        else:
            col = "black"
        canvas.create_rectangle(
            offset,
            300 - dimmensions_num[1],
            offset + dimmensions_num[0],
            300,
            outline=col,
            fill="grey",
        )
        offset += dimmensions_num[0]


canvas = tk.Canvas(win, width=800, height=400, bg="white")
canvas.pack()

delta_y = 60
entry = tk.Entry(
    win,
)
entry.pack()

button = tk.Button(win, text="Draw boundaries", command=get_delta_y)
button.pack()

draw()

win.mainloop()
