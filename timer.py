import tkinter as tk
import random

win = tk.Tk()
i = 0
body = 0
colours = ["cyan", "orange", "blue", "limegreen", "purple", "magenta", "red"]


def repeater():
    global i
    i += 1
    print(f"Hello {i}")
    canvas.after(1000, repeater)


def color_randomiser():
    colour = random.choice(colours)
    canvas.itemconfig(stvorec, fill=colour)
    print(canvas.itemcget(stvorec, "fill"))
    print(canvas.coords(stvorec))
    x = random.randrange(0, 250)
    y = random.randrange(0, 250)
    canvas.coords(stvorec, [x, y, x + 100, y + 100])
    canvas.after(1000, color_randomiser)


def bod(event):
    global body
    if canvas.itemcget(stvorec, "fill") == "red":
        body += 1
        canvas.itemconfig(text, text=f"pocet bodov: {body}")


colour = "black"
canvas = tk.Canvas(win, height=500, width=500, bg="hotpink")
stvorec = canvas.create_rectangle(100, 100, 300, 300, fill=colour)
text = canvas.create_text(250, 400, fill="white", text="pocet bodov: ")
color_randomiser()
repeater()
canvas.tag_bind(stvorec, "<Button-1>", bod)
canvas.pack()
win.mainloop()
