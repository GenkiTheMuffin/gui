import tkinter as tk

win = tk.Tk()
canvas = tk.Canvas(win, width=500, height=500, bg="white")
canvas.pack()


def draw_graph(shades, scale):
    for i in range(256):
        canvas.create_rectangle(
            i * 2,
            490,
            i * 2 + 2,
            490 - shades[i] / scale,
            width=0,
            fill="grey",
            outline="black",
        )


input_file = open("ciernobiely_obrazok_2.txt")
line = input_file.readline()
size = line.split()
width = int(size[0])
height = int(size[1])

shades = [0] * 256

for line in input_file:
    for i in range(width):
        shade = int(line[i * 2 : i * 2 + 2], 16)
        shades[shade] += 1
most_frequent = max(shades)
scale = most_frequent / 500 + 1

draw_graph(shades, scale)
win.mainloop()
