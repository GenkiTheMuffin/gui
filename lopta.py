from random import randrange
import tkinter as tk

x = randrange(0, 480)
y = randrange(0, 480)
offset_x = 10
offset_y = 10
win = tk.Tk()


def mover():
    global offset_y, offset_x
    if canvas.coords(lopta)[2] > 500:
        offset_x = -1 * offset_x
        offset_y = 1 * offset_y
    if canvas.coords(lopta)[0] <= 0:
        offset_x = -1 * offset_x
        offset_y = 1 * offset_y
    if canvas.coords(lopta)[3] > 500:
        offset_y = -1 * offset_y
        offset_x = 1 * offset_x
    if canvas.coords(lopta)[1] <= 0:
        offset_x = offset_x * 1
        offset_y = offset_y * -1
    canvas.move(lopta, offset_x, offset_y)
    canvas.after(10, mover)


canvas = tk.Canvas(height=500, width=500, bg="hotpink")
lopta = canvas.create_oval(x, y, x + 20, y + 20, fill="white")

mover()
canvas.pack()
win.mainloop()
